<aside class="sidebar">
    <div class="sidebar__wrap">
        <a class="sidebar__logo" href="/">
            <img src="img/logo.png" class="img-fluid" alt="">
        </a>
        <div class="sidebar__group">
            <div class="sidebar__title">Main</div>
            <ul class="sidebar__nav">
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__dashboard" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 478.703 478.703" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__settings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Settings</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="sidebar__group">
            <div class="sidebar__title">Components</div>
            <ul class="sidebar__nav">
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 481 481" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__money_bag" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Refill</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__funds" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Settings</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 511" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__biology" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Direct structure</span>
                    </a>
                </li>
                <li class="dropdowh">
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__menu" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Direct structure</span>
                        <i class="fa fa-angle-right"></i>
                    </a>

                    <ul class="sidebar__nav_second">
                        <li><a href="#">Matrix 25 structure</a></li>
                        <li><a href="#">Matrix 50 structure</a></li>
                        <li><a href="#">Matrix 100 structure</a></li>
                        <li><a href="#">Matrix 250 structure</a></li>
                        <li><a href="#">Matrix 500 structure</a></li>
                        <li><a href="#">Matrix 1000 structure</a></li>
                    </ul>

                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__bar_chart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Levels</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__history" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>History</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512.005 512.005" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__tickets" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Tickets</span>
                        <strong>2</strong>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>