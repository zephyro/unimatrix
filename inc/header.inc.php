<div class="header">
    <div class="header__container">
        <a class="header__toggle sidebar_toggle" href="#">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <ul class="header__content">
            <li>
                <div class="header__notification">
                    <a class="header__notification_label" href="#">
                        <span>3</span>
                        <i>
                            <svg class="ico-svg" viewBox="0 0 229.238 229.238" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__notification" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </a>
                    <div class="header__notification_dropdown">
                        <ul>
                            <li>
                                <a href="#">
                                    <strong>Your order is placed</strong>
                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <strong>Your order is placed</strong>
                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                </a>
                            </li>
                        </ul>
                        <a class="header__notification_view" href="#">View all</a>
                    </div>
                </div>
            </li>
            <li>
                <a class="header__auth" href="#">
                    <i>
                        <svg class="ico-svg" viewBox="0 0 24.303 24.303" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__power" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>

    </div>
</div>