<div class="widgets_group">

    <div class="widget">
        <div class="widget__icon">
            <i>
                <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </div>
        <div class="widget__heading">BALANCE</div>
        <div class="widget__data">
            <strong>$46,782</strong>
        </div>
        <div class="widget__info">
            <span class="widget__info_value">+11%</span>
            <span class="widget__info_legend">From previous period</span>
        </div>
    </div>

    <div class="widget">
        <div class="widget__icon">
            <i>
                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </div>
        <div class="widget__heading">SALES</div>
        <div class="widget__data">
            <strong>$0</strong>
            <span>l:0$, c:0$, r:0$</span>
        </div>
        <div class="widget__info">
            <span class="widget__info_value widget__info_value_rose">-28%</span>
            <span class="widget__info_legend">From previous period</span>
        </div>
    </div>

    <div class="widget">
        <div class="widget__icon">
            <i>
                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </div>
        <div class="widget__heading">REF. INCOME</div>
        <div class="widget__data">
            <strong>$14,132</strong>
        </div>
        <div class="widget__info">
            <span class="widget__info_value">+11%</span>
            <span class="widget__info_legend">From previous period</span>
        </div>
    </div>

    <div class="widget">
        <div class="widget__icon">
            <i>
                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </div>
        <div class="widget__heading">BONUS</div>
        <div class="widget__data">
            <strong>$2,332</strong>
        </div>
        <div class="widget__info">
            <span class="widget__info_value">+11%</span>
            <span class="widget__info_legend">From previous period</span>
        </div>
    </div>

</div>