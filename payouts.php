<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Refill</h1>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>

                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

                <h3 class="border_bottom">PLease, select payout method</h3>

                <div class="row">

                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                        <div class="ibox">
                            <div class="ibox__title">
                                <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                                <span class="ibox__title_text">Bitcoin</span>
                                <div class="ibox__title_data">Current BTC rate: <span class="color_yellow">3713.64$</span></div>
                            </div>
                            <div class="ibox__content">
                                <div class="row form_group">
                                    <div class="col col-xs-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                        <label class="form_label">Please enter your BTC wallet</label>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-xl-8 col-gutter-lr">
                                        <input class="form_control" name="" type="text" placeholder="Your BTC wallet" value="">
                                    </div>
                                </div>
                                <div class="row form_group">
                                    <div class="col col-xs-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                        <label class="form_label">Please, enter amount(USD)</label>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-xl-8 col-gutter-lr">
                                        <input class="form_control" name="" type="text" placeholder="Amount" value="">
                                    </div>
                                </div>
                                <div class="form_group">
                                    <button type="button" class="btn btn_long">Get payout</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                        <div class="ibox">
                            <div class="ibox__title">
                                <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                                <span class="ibox__title_text">Advcash</span>
                            </div>
                            <div class="ibox__content">
                                <div class="row form_group">
                                    <div class="col col-xs-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                        <label class="form_label">Please enter your AdvCash email</label>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-xl-8 col-gutter-lr">
                                        <input class="form_control" name="" type="text" placeholder="Your AdvCash wallet" value="">
                                    </div>
                                </div>
                                <div class="row form_group">
                                    <div class="col col-xs-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                        <label class="form_label">Please, enter amount</label>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-xl-8 col-gutter-lr">
                                        <input class="form_control" name="" type="text" placeholder="Amount" value="">
                                    </div>
                                </div>
                                <div class="form_group">
                                    <button type="button" class="btn btn_long">Get payout</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </div>
    </section>

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>