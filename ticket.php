<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Ticket: <span class="color_yellow">First ticket</span></h1>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>

                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

               <div class="tickets_nav">
                   <a href="#" class="tickets_nav__back">
                       <i class="fa fa-arrow-left"></i>
                       <span>Back to tickets list</span>
                   </a>
                   <a href="#" class="tickets_nav__close">
                       <i class="fa fa-close"></i>
                       <span>Close ticket</span>
                   </a>
               </div>

                <div class="ibox">
                    <div class="ibox__title">
                        <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                        <span class="ibox__title_text">New message</span>
                    </div>
                    <div class="ibox__content">
                        <form class="form">
                            <div class="form_group">
                                <textarea class="form_control" name="message" placeholder="Message" rows="4"></textarea>
                            </div>
                            <div class="row form_group">
                                <div class="col col-xs-7 col-sm-6 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                    <button type="submit" class="btn btn_long">Create ticket</button>
                                </div>
                                <div class="col col-xs-5 col-sm-4  col-md-3  col-lg-2 col-xl-2 col-gutter-lr">
                                    <button type="reset" class="btn btn_border btn_long">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="message message_answer">
                    <div class="message__author"><span>Support</span></div>
                    <div class="message__text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum ullamcorper eros et molestie. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur quis odio vulputate, tempus libero sit amet, porttitor arcu. Donec interdum mauris ut eros facilisis, semper ultricies nulla faucibus. Morbi quis gravida metus. Nunc venenatis vulputate nisi sit amet gravida. Donec auctor laoreet lacus eget tincidunt. Nullam et odio bibendum, scelerisque lacus a, maximus nibh. Aenean ut augue maximus, consequat massa vitae, pellentesque diam. Donec nec ipsum vehicula, eleifend lorem at, luctus sem. Curabitur quis magna
                    </div>
                </div>

                <div class="message">
                    <div class="message__author"><span>Alex Klinch</span></div>
                    <div class="message__text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum ullamcorper eros et molestie. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur quis odio vulputate, tempus libero sit amet, porttitor arcu.
                    </div>
                </div>

            </div>

        </div>
    </section>

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>