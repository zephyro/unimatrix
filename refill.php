<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <div class="heading">
                            <div class="heading__title">
                                <h1>Matrix <span class="color_yellow">25</span> structure</h1>
                            </div>
                            <div class="heading__data">
                                <div class="heading__data_elem">
                                    <span class="data_label">Direct partners</span>
                                    <span class="data_value">1237</span>
                                </div>
                                <div class="heading__data_elem">
                                    <span class="data_label">Matrix partners</span>
                                    <span class="data_value">162</span>
                                </div>
                            </div>

                        </div>

                        <!-- Widget -->
                        <?php include('inc/widgets.inc.php') ?>
                        <!-- -->

                        <h3 class="border_bottom">PLease, select payment method</h3>

                        <div class="row">
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="ibox">
                                    <div class="ibox__title">
                                        <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                                        <span class="ibox__title_text">Bitcoin</span>
                                    </div>
                                    <div class="ibox__content">
                                        <div class="row form_group">
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                                <label class="form_label">Current BTC rate: <span class="color_yellow">3724.89$</span></label>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 col-gutter-lr">
                                                <input class="form_control" name="" type="text" placeholder="" value="1925">
                                            </div>
                                        </div>
                                        <div class="row form_group mb_30">
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                                <label class="form_label">Please, send BTC</label>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 col-gutter-lr">
                                                <input class="form_control" name="" type="text" placeholder="" value="0.51679419">
                                            </div>
                                        </div>
                                        <div class="form_label mb_20">to this wallet</div>
                                        <div class="form_group">
                                            <div class="qr">
                                                <img src="images/qr.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <input type="text" name="mame" class="form_control text-center" placeholder="" value="37eMhdxQ1kh29kTEAt5krpRbd3qFY2wetS" disabled="">
                                        </div>
                                        <div class="form_group form_text">After money sending, please wait 10-30 minutes while the payment will appear on the blockchain.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="ibox">
                                    <div class="ibox__title">
                                        <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                                        <span class="ibox__title_text">Advcash</span>
                                    </div>
                                    <div class="ibox__content">
                                        <div class="row form_group">
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                                <label class="form_label">Please send USD to this wallet</label>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 col-gutter-lr">
                                                <input class="form_control" name="" type="text" placeholder="" value="U478987a977148">
                                            </div>
                                        </div>
                                        <div class="row form_group">
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 col-gutter-lr align_center">
                                                <label class="form_label">After you sent money, please</label>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 col-gutter-lr">
                                                <input class="form_control" name="" type="text" placeholder="" value="0.51679419">
                                            </div>
                                        </div>
                                        <div class="row form_group">
                                            <div class="col col-xs-12 col-gutter-lr">
                                                <button type="button" class="btn btn_long">Check payment</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>


