<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->



        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Tickets</h1>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>

                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

                <h3 class="border_bottom">Add new ticket or watch other tickets</h3>

                <div class="ibox">
                    <div class="ibox__title">
                        <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                        <span class="ibox__title_text">New ticket</span>
                    </div>
                    <div class="ibox__content">
                        <form class="form">
                            <div class="form_group">
                                <input type="text" class="form_control" name="name" placeholder="Title">
                            </div>
                            <div class="form_group">
                                <textarea class="form_control" name="message" placeholder="Message" rows="4"></textarea>
                            </div>
                            <div class="row form_group">
                                <div class="col col-xs-7 col-sm-6 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                    <button type="submit" class="btn btn_long">Create ticket</button>
                                </div>
                                <div class="col col-xs-5 col-sm-4  col-md-3  col-lg-2 col-xl-2 col-gutter-lr">
                                    <button type="reset" class="btn btn_border btn_long">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table_responsive">
                    <table class="table">
                        <tr>
                            <th class="hide-xs-only">Date</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td class="text-nowrap hide-xs-only">2019-01-19 16:02:31</td>
                            <td><a href="#">Best ticket ever you senn today ago forever young</a></td>
                            <td><span class="status_ok">Closed</span></td>
                            <td><a class="btn btn_border btn_sm">VIEW</a></td>
                        </tr>
                        <tr>
                            <td class="text-nowrap hide-xs-only">2019-01-19 16:02:31</td>
                            <td><a href="#">Best ticket ever you senn today ago forever young</a></td>
                            <td><span class="status_ok">Closed</span></td>
                            <td><a class="btn btn_border btn_sm">VIEW</a></td>
                        </tr>
                        <tr>
                            <td class="text-nowrap hide-xs-only">2019-01-19 16:02:31</td>
                            <td><a href="#">Best ticket ever you senn today ago forever young</a></td>
                            <td><span class="status_ok">Closed</span></td>
                            <td><a class="btn btn_border btn_sm">VIEW</a></td>
                        </tr>
                        <tr>
                            <td class="text-nowrap hide-xs-only">2019-01-19 16:02:31</td>
                            <td><a href="#">Best ticket ever you senn today ago forever young</a></td>
                            <td><span class="status_ok">Closed</span></td>
                            <td><a class="btn btn_border btn_sm">VIEW</a></td>
                        </tr>
                        <tr>
                            <td class="text-nowrap hide-xs-only">2019-01-19 16:02:31</td>
                            <td><a href="#">Best ticket ever you senn today ago forever young</a></td>
                            <td><span class="status_ok">Closed</span></td>
                            <td><a class="btn btn_border btn_sm">VIEW</a></td>
                        </tr>
                    </table>
                </div>

            </div>

        </div>
    </section>

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>