<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <div class="heading">
                            <div class="heading__title">
                                <h1>History</h1>
                            </div>
                            <div class="heading__data">
                                <div class="heading__data_elem">
                                    <span class="data_label">Direct partners</span>
                                    <span class="data_value">1237</span>
                                </div>
                                <div class="heading__data_elem">
                                    <span class="data_label">Matrix partners</span>
                                    <span class="data_value">162</span>
                                </div>
                            </div>

                        </div>

                        <!-- Widget -->
                        <?php include('inc/widgets.inc.php') ?>
                        <!-- -->

                        <div class="table_sort">
                            <div class="table_sort__elem">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="sort" value="">
                                    <span>Refill</span>
                                </label>
                            </div>
                            <div class="table_sort__elem">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="sort" value="">
                                    <span>Payout</span>
                                </label>
                            </div>
                            <div class="table_sort__elem">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="sort" value="">
                                    <span>Refferal income</span>
                                </label>
                            </div>
                            <div class="table_sort__elem">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="sort" value="">
                                    <span>Matching bonus</span>
                                </label>
                            </div>
                            <div class="table_sort__elem">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="sort" value="">
                                    <span>Buy matrix</span>
                                </label>
                            </div>
                        </div>

                        <div class="table_responsive mb_30">
                            <table class="table">

                                <tr>
                                    <th>Date</th>
                                    <th>Operation</th>
                                    <th>Amount</th>
                                    <th class="text-center">Status</th>
                                </tr>

                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>
                                <tr>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                    <td>Matching bonus, gumenyukpavel09@gmail.com</td>
                                    <td>1237 USD</td>
                                    <td class="text-center"><span class="status_ok">Done</span></td>
                                </tr>

                            </table>
                        </div>

                        <div class="pagination">
                            <div class="pagination__text">Showing <span>1 to 20</span> of <span>57</span> entries</div>

                            <ul class="pagination__nav">
                                <li class="prev"><a href="#">PREVIOUS</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li class="next"><a href="#">NEXT</a></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
