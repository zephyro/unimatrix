// SVG IE11 support
svg4everybody();


(function() {

    $('.sidebar_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.page').toggleClass('sidebar_open');
    });

}());

(function() {

    $('.dropdowh > a').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.dropdowh').toggleClass('open');
        $(this).closest('.dropdowh').find('.sidebar__nav_second').slideToggle('fast');
    });

}());


$(".btn_modal").fancybox({
    'padding'    : 0
});


$(".sidebar__wrap").mCustomScrollbar({
    theme:"inset-dark",
    scrollInertia:700
});





$('.ibox__title_icon, .ibox__title_text').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.ibox').toggleClass('close');
    $(this).closest('.ibox').find('.ibox__content').slideToggle('fast');
});


$('.header__notification_label').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.header__notification').toggleClass('open');
});

// hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".header__notification_label").length === 0) {
        $(".header__notification").removeClass('open');
    }
});

$('.info_block__close').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.info_block').remove();
});

