<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Dashboard</h1>
                        <div class="heading__text">Referal link: <a href="#">https://unixmatrix.com/r/U7800</a></div>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>
                </div>

                <div class="info_block">
                    <div class="info_block__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit: Alert link</div>
                    <span class="info_block__close"></span>
                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

                <h3 class="border_bottom">Choose your plan</h3>

                <div class="dashboard">

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 1</div>
                        <div class="dashboard__elem_value">$25</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 2</div>
                        <div class="dashboard__elem_value">$50</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 3</div>
                        <div class="dashboard__elem_value">$100</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 4</div>
                        <div class="dashboard__elem_value">$250</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 5</div>
                        <div class="dashboard__elem_value">$500</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 6</div>
                        <div class="dashboard__elem_value">$100</div>
                        <div class="dashboard__elem_bottom">
                            <a class="btn btn_border btn_sm btn_long btn_modal" href="#order">BUY</a>
                        </div>
                    </div>

                </div>

                <h3 class="border_bottom">Your matrices</h3>

                <div class="dashboard">

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 1</div>
                        <div class="dashboard__elem_value">$25</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 2</div>
                        <div class="dashboard__elem_value">$50</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 3</div>
                        <div class="dashboard__elem_value">$100</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 4</div>
                        <div class="dashboard__elem_value">$250</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 5</div>
                        <div class="dashboard__elem_value">$500</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                    <div class="dashboard__elem">
                        <div class="dashboard__elem_title">MATRIX 6</div>
                        <div class="dashboard__elem_value">$100</div>
                        <div class="dashboard__elem_bottom">
                            <span class="status_ok">2019-01-16</span>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>

</div>

<div class="hide">
    <div class="modal" id="order">
        <div class="modal__title">Do you want to buy this Matrix?</div>
        <div class="modal__content">
            <div class="row">
                <div class="col col-xs-6 col-gutter-lr">
                    <button type="button" class="btn btn_long">Yes</button>
                </div>
                <div class="col col-xs-6 col-gutter-lr">
                    <button type="button" class="btn btn_border btn_long">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>