<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Matrix <span class="color_yellow">25</span> structure</h1>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>

                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

                <div class="row mb_30">

                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">

                        <h3 class="border_bottom">USER INFO</h3>

                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">NAME</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">SECOND NAME</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">COUNTRY</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center"></div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <button type="button" class="btn btn_long">Safe info</button>
                            </div>
                        </div>

                    </div>

                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">

                        <h3 class="border_bottom">Change password</h3>

                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">New pass</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">Repeat password</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center">
                                <label class="form_label">Current password</label>
                            </div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <input class="form_control" name="" type="text" placeholder="" value="">
                            </div>
                        </div>
                        <div class="row form_group">
                            <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr align_center"></div>
                            <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr">
                                <button type="button" class="btn btn_long">SSave password</button>
                            </div>
                        </div>

                    </div>
                </div>

                <h3 class="border_bottom">Security</h3>

                <div class="row">
                    <div class="col col-xs-12 col-md-6 col-gutter-lr">


                        <div class="ibox">

                            <div class="ibox__title">
                                <span class="ibox__title_icon"><i class="fa fa-angle-up"></i></span>
                                <span class="ibox__title_text">2FA Enable</span>
                            </div>

                            <div class="ibox__content">

                                <div class="form_group">
                                    <div class="qr">
                                        <img src="images/qr.png" class="img-fluid" alt="">
                                    </div>
                                </div>

                                <div class="row form_group">
                                    <div class="col col-xs-12 col-sm-5 col-xl-4 col-gutter-lr align_center">
                                        <label class="form_label">Enter 2fa code</label>
                                    </div>
                                    <div class="col col-xs-12 col-sm-7 col-xl-8 col-gutter-lr">
                                        <input class="form_control" name="" type="text" placeholder="" value="0.51679419">
                                    </div>
                                </div>

                                <div class="form_group">
                                    <input type="text" name="mame" class="form_control text-center" placeholder="" value="37eMhdxQ1kh29kTEAt5krpRbd3qFY2wetS" disabled="">
                                </div>

                                <label class="form_label mb_10">Important: Print a backup</label>

                                <div class="row form_group">
                                    <div class="col col-xs-6">
                                        <button type="submit" class="btn btn_long">Enable 2FA</button>
                                    </div>
                                    <div class="col col-xs-6">
                                        <button type="submit" class="btn btn_border btn_long">Cancel</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>