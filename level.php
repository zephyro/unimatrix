<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Sidebar -->
    <?php include('inc/sidebar.inc.php') ?>
    <!-- -->

    <section class="main">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="content">

            <div class="container">

                <div class="heading">
                    <div class="heading__title">
                        <h1>Levels</h1>
                    </div>
                    <div class="heading__data">
                        <div class="heading__data_elem">
                            <span class="data_label">Direct partners</span>
                            <span class="data_value">1237</span>
                        </div>
                        <div class="heading__data_elem">
                            <span class="data_label">Matrix partners</span>
                            <span class="data_value">162</span>
                        </div>
                    </div>

                </div>

                <!-- Widget -->
                <?php include('inc/widgets.inc.php') ?>
                <!-- -->

                <div class="level_responsive">

                    <div class="level">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__black.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">7 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Car</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                    <div class="level">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__black.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">6 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Lux Apartments</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                    <div class="level">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__black.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">5 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Business classCar</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                    <div class="level">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__black.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">4 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Car</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                    <div class="level level_one">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__black.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_value">60%</div>
                            <div class="level__heading_name">3 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Car</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                        <div class="level__scale"></div>
                        <div class="level__line" style="width: 60%;"></div>
                    </div>

                    <div class="level level_two">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__yellow.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">2 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Car</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                    <div class="level level_two">
                        <div class="level__heading">
                            <div class="level__heading_image">
                                <img src="img/level__yellow.png" class="img-fluid" alt="">
                            </div>
                            <div class="level__heading_name">1 LEVEL</div>
                            <div class="level__heading_text">MIDDLE DIRECTOR</div>
                        </div>

                        <div class="level__sale">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Sales</div>
                            <div class="level__data">$4,500,000</div>
                            <div class="level__sub">*32134</div>
                        </div>
                        <div class="level__partner">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">DIRECT PARTNERS</div>
                            <div class="level__data">50</div>
                        </div>
                        <div class="level__gift">
                            <div class="level__icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="level__title">Gift</div>
                            <div class="level__data">Car</div>
                        </div>
                        <div class="level__info">
                            <div class="level__info_title">REQUIREMENTS</div>
                            <div class="level__info_text">3 legs with a sales at least $500,000 each + 3 leaders who have completed of U3 level or 1 leader with a level U4 + 20 personally invited </div>
                        </div>
                        <div class="level__bg"></div>
                    </div>

                </div>

            </div>

        </div>
    </section>

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>
</html>