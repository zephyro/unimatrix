<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <div class="heading">
                            <div class="heading__title">
                                <h1>Matrix <span class="color_yellow">25</span> structure</h1>
                            </div>
                            <div class="heading__data">
                                <div class="heading__data_elem">
                                    <span class="data_label">Direct partners</span>
                                    <span class="data_value">1237</span>
                                </div>
                                <div class="heading__data_elem">
                                    <span class="data_label">Matrix partners</span>
                                    <span class="data_value">162</span>
                                </div>
                            </div>

                        </div>

                        <!-- Widget -->
                        <?php include('inc/widgets.inc.php') ?>
                        <!-- -->

                        <h3 class="border_bottom">Your STRUCTURE</h3>

                        <div class="row">
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="inline_box">
                                    <div class="inline_box__item">
                                        <div class="inline_box__icon">
                                            <svg class="ico-svg" viewBox="0 0 25.916 25.916" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__man" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <span class="inline_box__label">Partners count</span>
                                        <span class="inline_box__value">0</span>
                                    </div>
                                    <div class="inline_box__item">
                                        <div class="inline_box__icon">
                                            <svg class="ico-svg" viewBox="0 0 487.378 487.378" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__badge" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <span class="inline_box__label">SAles</span>
                                        <span class="inline_box__value">$0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="form_box">
                                    <input type="text" class="form_control" name="name" placeholder="User ID or E-mail">
                                    <button type="button" class="btn">Search</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
