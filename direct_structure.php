<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <div class="heading">
                            <div class="heading__title">
                                <h1>Direct structure</h1>
                            </div>
                            <div class="heading__data">
                                <div class="heading__data_elem">
                                    <span class="data_label">Direct partners</span>
                                    <span class="data_value">1237</span>
                                </div>
                                <div class="heading__data_elem">
                                    <span class="data_label">Matrix partners</span>
                                    <span class="data_value">162</span>
                                </div>
                            </div>

                        </div>

                        <!-- Widgets -->
                        <?php include('inc/widgets.inc.php') ?>
                        <!-- -->

                        <div class="table_responsive">
                            <table class="table">
                                <tr>
                                    <th>ID</th>
                                    <th>E-MAIL</th>
                                    <th>Name</th>
                                    <th>Registration date</th>
                                </tr>
                                <tr>
                                    <td>U7777</td>
                                    <td>gumenyukpavel09@gmail.com</td>
                                    <td>Pavel Gumenyuk</td>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                </tr>
                                <tr>
                                    <td>U7777</td>
                                    <td>gumenyukpavel09@gmail.com</td>
                                    <td>Pavel Gumenyuk</td>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                </tr>
                                <tr>
                                    <td>U7777</td>
                                    <td>gumenyukpavel09@gmail.com</td>
                                    <td>Pavel Gumenyuk</td>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                </tr>
                                <tr>
                                    <td>U7777</td>
                                    <td>gumenyukpavel09@gmail.com</td>
                                    <td>Pavel Gumenyuk</td>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                </tr>
                                <tr>
                                    <td>U7777</td>
                                    <td>gumenyukpavel09@gmail.com</td>
                                    <td>Pavel Gumenyuk</td>
                                    <td class="text-nowrap">2018-12-31 13:10:14</td>
                                </tr>
                            </table>
                        </div>

                    </div>

                </div>
            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
